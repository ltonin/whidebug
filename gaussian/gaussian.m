clear all; clc;

root  = [getenv('WHICORE') 'extra/gaussian/'];

Alpha         = 0.96;
Rejection     = 0.6;
Modality      = 'AsRecursive'; %'AsReplace';


frnd  = [root 'gaussian_classifier_rnd.dat'];
fpp   = [root 'gaussian_classifier_pp.dat'];
fipp  = [root 'gaussian_classifier_ipp.dat'];
frpp  = [root 'gaussian_classifier_rpp.dat'];
fgauc = [root 'gaussian_classifier.dat'];
fgaum = [root 'gaussian_classifier.mat'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('[io] - Load CNBI gaussian classifier');
[hdr, Mc, Cc] = wc_load_classifier(fgauc);

NumClasses      = hdr.nclasses;
NumPrototypes   = hdr.nprototypes;
NumFeatures     = hdr.nfeatures;
DefaultVal      = 1/NumClasses;

disp('[io] - Open random input signal');
rnd = wc_load(frnd, 'eigen', 'double');

disp('[io] - Open posterior probabilities');
pp = wc_load(fpp, 'eigen', 'double');

disp('[io] - Open rejected posterior probabilities');
rpp = wc_load(frpp, 'eigen', 'double');

disp('[io] - Open integrated posterior probabilities');
ipp = wc_load(fipp,  'eigen', 'double');

NumSamples = size(rnd, 1);

disp('[io] - Loading mat classifier');
gaum = load(fgaum);
Mm = gaum.analysis.tools.net.gau.M;
Cm = gaum.analysis.tools.net.gau.C;

disp(['[check] - Equality of means between WHITK and MATLAB classifiers: ' num2str(isequal(Mc, Mm))]);
disp(['[check] - Equality of covs  between WHITK and MATLAB classifiers: ' num2str(isequal(Cc, Cm))]);

disp('[proc] - Classification in MATLAB');
pp_m = zeros(NumSamples, NumClasses);

for sId = 1:NumSamples
    [~, pp_m(sId, :)] = gauClassifier(Mm, Cm, rnd(sId, :));
end

disp('[proc] - Rejection in MATLAB')
rpp_m = pp_m;
for sId = 1:NumSamples
    
    fillval = DefaultVal;
    
    if (strcmpi(Modality, 'AsRecursive') && sId > 1)
        fillval = rpp_m(sId - 1, :);
    end
    
    if(max(pp_m(sId, :)) < Rejection)
            rpp_m(sId, :) = fillval;
    end
end

disp('[proc] - Integration in MATLAB')
ipp_m = rpp_m;
for sId = 1:NumSamples
    if (sId == 1)
        prevprobs = [1/NumClasses 1/NumClasses];
    else
        prevprobs = ipp_m(sId - 1, :);
    end
    ipp_m(sId, :) = eegc3_expsmooth(prevprobs, rpp_m(sId, :), Alpha);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['[check] - Goodness of fit of Posterior  Probabilities: ' num2str(min(goodnessOfFit(pp, pp_m, 'NMSE')))]);
disp(['[check] - Goodness of fit of Rejected   Probabilities: ' num2str(min(goodnessOfFit(rpp, rpp_m, 'NMSE')))]);
disp(['[check] - Goodness of fit of Integrated Probabilities: ' num2str(min(goodnessOfFit(ipp, ipp_m, 'NMSE')))]);
