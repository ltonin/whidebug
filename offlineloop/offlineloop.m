clear all; clc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
root  = [getenv('WHISMR') 'extra/offlineloop/'];

BCI_PSD_GRID      = 0:2:256;
BCI_EXP_REJECTION = 0.6;
BCI_EXP_ALPHA     = 0.96;

fgdf    = [root 'offlineloop.gdf'];
fgau    = [root 'gaussian_classifier.mat'];
fpsd    = [root 'offlineloop_psd.dat'];
fpp     = [root 'offlineloop_pp.dat'];
frpp    = [root 'offlineloop_rpp.dat'];
fipp    = [root 'offlineloop_ipp.dat'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('[io] - Open psd');
[psd, hpsd] = wc_load(fpsd, 'eigen', 'double');

disp('[io] - Open posterior probabilities');
pp = wc_load(fpp, 'eigen', 'double');

disp('[io] - Open rejected posterior probabilities');
rpp = wc_load(frpp, 'eigen', 'double');

disp('[io] - Open integrated posterior probabilities');
ipp = wc_load(fipp, 'eigen', 'double');


NumSamples  = size(pp, 1);
NumClasses  = size(pp, 2);
NumChannels = size(psd, 2);
NumFFT      = length(BCI_PSD_GRID);

psd = wc_2dTo3d(psd, [NumFFT NumChannels NumSamples]);
psd_avg = mean(psd, 3); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


bci = cnbi_smr_simloop(fgdf, [], fgau, BCI_EXP_REJECTION, BCI_EXP_ALPHA);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


disp('[proc] - Computing goodness of fit for results');
disp('[proc] + Features');

[~, psd_id] = intersect(BCI_PSD_GRID, bci.analysis.settings.features.psd.freqs);
psd_c = permute(psd(psd_id, :, :), [3 1 2]);
psd_m = bci.afeats;
psd_m(isnan(psd_m)) = 0;
goF_psd = zeros(length(psd_id), NumChannels);

for fId = 1:length(psd_id)
    goF_psd(fId, :) = goodnessOfFit(squeeze(psd_c(fId, :, :)), squeeze(psd_m(fId, :, :)), 'NMSE');
end

disp('[proc] + Selected features');
feat_c = [];
feat_m = bci.nfeats;
for ch = bci.analysis.tools.features.channels
    bns = bci.analysis.tools.features.bands{ch}; 
    for bni = bns
        bn = find(bci.analysis.settings.features.psd.freqs == bni);
        feat_c = cat(2, feat_c, psd_c(:, bn, ch));
    end
end
feat_diff = feat_c - feat_m;

disp('[proc] + Raw probabilities');
rawpp_c = pp;
rawpp_m = bci.cprobs;
rawpp_m(isinf(rawpp_m)) = 0;
goF_rawpp  = goodnessOfFit(rawpp_c, rawpp_m, 'NMSE');

disp('[proc] + Probabilities with rejection');
rejpp_c = rpp;
rejpp_m = bci.rprobs;
rejpp_m(isinf(rejpp_m)) = 0;
goF_rejpp  = goodnessOfFit(rejpp_c, rejpp_m, 'NMSE');

disp('[proc] + Probabilities with integration');
intpp_c = ipp;
intpp_m = bci.iprobs;
intpp_m(isinf(intpp_m)) = 0;
goF_intpp  = goodnessOfFit(intpp_c, intpp_m, 'NMSE');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gcf = figure;
%fig_set_position(gcf, 'Top');

subplot(3, 3, [1 2]);
colormap(gray);
plot(BCI_PSD_GRID, psd_avg);
grid on;
xlim([0 BCI_PSD_GRID(end)]);
xlabel('Frequency [Hz]');
ylabel('Power');
title('Power spectrum computed with BCITK');

subplot(3, 3, 3);
colormap(flipud(gray));
imagesc(goF_psd');
colorbar('EastOutside');
xlabel('Frequency [Hz]');
ylabel('Channel');
title('Goodness of fit between PSDs');
plot_setCLim(gca, [0.9 1]);

subplot(3, 3, [4 5 6]);
colormap(flipud(gray));
imagesc(abs(1 - feat_diff)');
colorbar('EastOutside');
set(gca, 'YTick', 1:size(feat_diff, 2));
set(gca, 'YTickLabel', num2str((1:size(feat_diff, 2))'));
xlabel('Samples');
ylabel('Features');
title('1 - differences between features over time');
plot_setCLim(gca, [0.9 1]);

subplot(3, 3, 7);
colormap(gray);
bar(goF_rawpp);
ylim([0 1.2]);
grid on;
xlabel('Classes');
ylabel('NMSE [-inf 1]');
title('Goodness of fit between raw pp');
xlim([0 3]);

subplot(3, 3, 8);
bar(goF_rejpp);
ylim([0 1.2]);
grid on;
xlabel('Classes');
ylabel('NMSE [-inf 1]');
title('Goodness of fit between pp with rejection');
xlim([0 3]);

subplot(3, 3, 9);
bar(goF_intpp);
ylim([0 1.2]);
grid on;
xlabel('Classes');
ylabel('NMSE [-inf 1]');
title('Goodness of fit between pp with integration');
xlim([0 3]);



