clear all; clc;
root  = [getenv('WHICORE') 'extra/data/windows/'];

frndm = [root 'windows_rndm.dat'];
fhann = [root 'windows_hann.dat'];
fhamm = [root 'windows_hamm.dat'];
ffltp = [root 'windows_fltp.dat'];
fblkm = [root 'windows_blkm.dat'];

disp('[io] - Open random input signal');
[wrndm, hrndm] = wc_binary_load(frndm, 'double');

disp('[io] - Open Hann window');
[whann, hhann] = wc_binary_load(fhann, 'double');

disp('[io] - Open Hamming window');
[whamm, hhamm] = wc_binary_load(fhamm, 'double');

disp('[io] - Open FlatTop window');
[wfltp, hfltp] = wc_binary_load(ffltp, 'double');

disp('[io] - Open Blackman window');
[wblkm, hblkm] = wc_binary_load(fblkm, 'double');

NumSamples  = size(wrndm, 1);
NumChannels = size(wrndm, 2);

disp('==========================');
disp('Applying matlab windows');
disp('==========================');

whann_m = wrndm .* repmat(hann(NumSamples), [1 NumChannels]);
whamm_m = wrndm .* repmat(hamming(NumSamples), [1 NumChannels]);
wfltp_m = wrndm .* repmat(flattopwin(NumSamples), [1 NumChannels]);
wblkm_m = wrndm .* repmat(blackman(NumSamples), [1 NumChannels]);

disp('==========================');
disp('Check goodness of fit');
disp('==========================');

disp('[chk] - Hann Window');
for chId = 1:NumChannels
    disp(['[chk] - Goodness of fit for channel ' num2str(chId) ': ' num2str(goodnessOfFit(whann(:, chId), whann_m(:, chId), 'NMSE'))]);
end

disp('[chk] - Hamming Window');
for chId = 1:NumChannels
    disp(['[chk] - Goodness of fit for channel ' num2str(chId) ': ' num2str(goodnessOfFit(whamm(:, chId), whamm_m(:, chId), 'NMSE'))]);
end

disp('[chk] - FlatTop Window');
for chId = 1:NumChannels
    disp(['[chk] - Goodness of fit for channel ' num2str(chId) ': ' num2str(goodnessOfFit(wfltp(:, chId), wfltp_m(:, chId), 'NMSE'))]);
end

disp('[chk] - Blackman Window');
for chId = 1:NumChannels
    disp(['[chk] - Goodness of fit for channel ' num2str(chId) ': ' num2str(goodnessOfFit(wblkm(:, chId), wblkm_m(:, chId), 'NMSE'))]);
end