clear all; clc;
root  = [getenv('WHICORE') 'extra/data/readwrite/'];

fdat = [root 'readwrite_rnd.dat'];

disp('[io] - Open random data');
[srnd, hrnd] = wc_binary_load(fdat, 'double');

disp(hrnd);