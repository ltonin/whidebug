clear all; clc;
root  = [getenv('WHICORE') 'extra/filters/'];

frnd  = [root 'filters_rnd.dat'];
fdc  = [root 'filters_dc.dat'];
fcar = [root 'filters_car.dat'];
flap = [root 'filters_lap.dat'];
fmsk = [getenv('WHICORE') 'extra/lapmask_16ch.dat'];

disp('[io] - Open random input signal');
rnd = wc_load(frnd, 'eigen', 'double');

disp('[io] - Open dc signal');
dc = wc_load(fdc, 'eigen', 'double');

disp('[io] - Open car signal');
car = wc_load(fcar, 'eigen', 'double');

disp('[io] - Open lap signal');
lap = wc_load(flap, 'eigen', 'double');

disp('[io] - Open lap mask');
msk = wc_load(fmsk, 'eigen', 'double');

NumChannels = size(rnd, 2);

disp('==========================');
disp('Applying matlab filters');
disp('==========================');

dc_m    = proc_dc(rnd);
car_m   = proc_car(rnd);
lap_m   = rnd * msk;

disp('==========================');
disp('Check goodness of fit');
disp('==========================');

disp('[chk] - DC filter');
for chId = 1:NumChannels
    disp(['[chk] - Goodness of fit for channel ' num2str(chId) ': ' num2str(goodnessOfFit(dc(:, chId), dc_m(:, chId), 'NMSE'))]);
end

disp('[chk] - CAR filter');
for chId = 1:NumChannels
    disp(['[chk] - Goodness of fit for channel ' num2str(chId) ': ' num2str(goodnessOfFit(car(:, chId), car_m(:, chId), 'NMSE'))]);
end

disp('[chk] - LAP filter');
for chId = 1:NumChannels
    disp(['[chk] - Goodness of fit for channel ' num2str(chId) ': ' num2str(goodnessOfFit(lap(:, chId), lap_m(:, chId), 'NMSE'))]);
end