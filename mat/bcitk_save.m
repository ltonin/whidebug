function rdata = bcitk_save(filename, data, mode, datatype)
    
    if nargin < 3
        mode = 'w';
        datatype = 'double';
    end
    
    if nargin < 4
        datatype = 'double';
    end
    
    if isempty(mode)
        mode = 'w';
    end

    ndim = ndims(data);
    
    if ndim > 3
        error('chk:dim', 'Maximum 3 dimensions are allowed');
    end
    
    if ndim == 3
        rdata = [];
        for sliceId = 1:size(data, 3)
            rdata = cat(1, rdata, data(:, :, sliceId));
        end
    else
        rdata = data;
    end

    % Try to open the file
    fid = fopen(filename, mode);
    
    % Check for errors
    if fid < 0
        error('chk:file', ['Impossible to open file at: ' filename]);
    end
    
    % Write data
    fwrite(fid, rdata, datatype);
    fclose(fid);

end