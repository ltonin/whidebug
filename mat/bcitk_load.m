function data = bcitk_load(filename, dim, datatype)

    if nargin < 3
        datatype = 'double';
    end

    ndim = length(dim);
    
    if ndim > 3
        error('chk:dim', 'Maximum 3 dimensions are allowed');
    end
    
    rdim = dim;
    
    if ndim == 3
        rdim = [dim(1)*dim(3) dim(2)];
    end
    
    % Try to open the file
    fid = fopen(filename, 'r+');
    
    % Check for errors
    if fid < 0
        error('chk:file', ['Impossible to open file at: ' filename]);
    end
    
    % Read file
    data_raw  = fread(fid, rdim, datatype);
    
    % Close file
    fclose(fid);
    
    % In the case of 3D matrix, reshape the data. We assume that the
    % third dimension is stored in the file as additional rows
    
    
    if ndim == 3
        data = zeros(dim);
        for dId = 1:dim(3)
            data(:, :, dId) = data_raw((dId - 1)*dim(1) + 1:dId*dim(1), :);
        end
    else
        data = data_raw;
    end

end