clear all; close all;

datapath_mat    = '/mnt/data/Git/Codes/bcitk/extra/data/test_cvsa_psd_matlab.dat';
datapath_c      = '/mnt/data/Git/Codes/bcitk/extra/data/test_cvsa_psd.dat';
gridpath_c      = '/mnt/data/Git/Codes/bcitk/extra/data/test_cvsa_grid.dat';

SAMPLES     = 5568;       % Number of frames acquired with simulated loop
NCHANNELS   = 64;         % Number of eeg channels
FRQS_MAT    = 4:2:48;     % Frequency grid for matlab psd
NFREQS_MAT  = length(FRQS_MAT);
NFREQS_C    = 129;

disp('[bcitest] Loading MATLAB and C data');
data_mat = bcitk_load(datapath_mat, [SAMPLES NFREQS_MAT NCHANNELS]);
data_c   = bcitk_load(datapath_c, [NFREQS_C NCHANNELS SAMPLES]);
grid_c   = bcitk_load(gridpath_c, [1 NFREQS_C], 'int');


disp('[bcitest] Extract frequency from c data');
[~, idfq] = intersect(grid_c, FRQS_MAT);
grid = grid_c(idfq);
data_c = data_c(idfq, :, :);

disp('[bcitest] Reshape dataset');
psd_c = zeros(size(data_c, 3), size(data_c, 1), size(data_c, 2));
for s = 1:size(data_c, 3)
    psd_c(s, :, :) = data_c(:, :, s);
end
psd_m = data_mat;



disp('[bcitest] Check goodness of fit between MATLAB and C psd');
gdf = zeros(size(psd_c, 1), size(psd_c, 3));
for sId = 1:size(psd_c, 1)
    for chId = 1:size(psd_c, 3)
        cpsd_c = squeeze(psd_c(sId, :, chId));
        cpsd_m = squeeze(psd_m(sId, :, chId));
        gdf(sId, chId) = goodnessOfFit(cpsd_c', cpsd_m', 'NMSE');
    end
end


