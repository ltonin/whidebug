function [P, clf] = bcitk_mat2bin_classifier(ifile, ofile, type)

    if nargin < 3
        type = 'cnbi-gaussian';
    end

    switch lower(type)
        case 'cnbi-gaussian'
            disp(['Exporting cnbi gaussian classifier (' ifile ') to: ' ofile]); 
            export_cnbi_gaussian(ifile, ofile);
            %[P, clf] = convert_cnbi_gaussian(ifile);
            %bcitk_save(ofile, P);
        
        otherwise
            error('chk:typ', 'Classifier type not recognized');
    end
end

function export_cnbi_gaussian(ifile, ofile)

    load(ifile);
    
    % Get header parameters
    format  = 'gaussian';
    type    = 'gaussian';
    label   = 'gaussian';
    subject = analysis.info.subject;
    
    nclas   = size(analysis.tools.net.gau.M, 1);
    nprot   = size(analysis.tools.net.gau.M, 2);
    nfeat   = size(analysis.tools.net.gau.M, 3);
    
    somunit = analysis.settings.classification.gau.somunits;
    shrdcov = analysis.settings.classification.gau.sharedcov;
    minmean = analysis.settings.classification.gau.mimean;
    mincov  = analysis.settings.classification.gau.micov;

    [featch, featfr] = getfeatures_cnbi_gaussian(analysis.tools.features);
  
    % Get data (mean and covariance)
    D = getdata_cnbi_gaussian(analysis.tools.net.gau.M, ...
                              analysis.tools.net.gau.C);
                          
    
                          
    % Open file
    fid = fopen(ofile, 'w+', 'l', 'UTF-8');
    if(fid==-1), error(['error: can''t open file: ' ofile '\n']); end
    fseek(fid,0,'bof');
    
    % Write header
    fwrite(fid, numel(format), 'uint32');
    %fwrite(fid, format, 'char');
    fprintf(fid, '%s', format);
    fwrite(fid, numel(type), 'uint32');
    %fwrite(fid, type, 'char');
    fprintf(fid, '%s', type);
    fwrite(fid, numel(label), 'uint32');
    %fwrite(fid, label, 'char');
    fprintf(fid, '%s', label);
    fwrite(fid, numel(subject), 'uint32');
    %fwrite(fid, subject, 'char');
    fprintf(fid, '%s', subject);
    
    fwrite(fid, numel(nclas), 'uint32');
    fwrite(fid, nclas, 'double');
    fwrite(fid, numel(nprot), 'uint32');
    fwrite(fid, nprot, 'double');
    fwrite(fid, numel(nfeat), 'uint32');
    fwrite(fid, nfeat, 'double');
    
    fwrite(fid, numel(somunit), 'uint32');
    fwrite(fid, somunit, 'double');
    fwrite(fid, numel(shrdcov), 'char');
    fwrite(fid, shrdcov, 'char');
    fwrite(fid, numel(minmean), 'uint32');
    fwrite(fid, minmean, 'double');
    fwrite(fid, numel(mincov), 'uint32');
    fwrite(fid, mincov, 'double');
    
    fwrite(fid, numel(featch), 'uint32');
    fwrite(fid, featch, 'double');
    fwrite(fid, numel(featfr), 'uint32');
    fwrite(fid, featfr, 'double');
    
    fclose(fid);
    
end

function [chans, freqs] = getfeatures_cnbi_gaussian(strfeatures)
    
    chans = [];
    freqs = [];
    chIndex = strfeatures.channels;
    for chId = 1:length(chIndex)
        currChId = chIndex(chId);
        freqIndex = strfeatures.bands{currChId};
       
        chans = cat(1, chans, repmat(currChId, length(freqIndex), 1));
        freqs = cat(1, freqs, freqIndex');
    end


end

function [P, clf] = getdata_cnbi_gaussian(M, C)
    
    Mp = permute(M, [3 2 1]);
    Cp = permute(C, [3 2 1]);

    P = cat(3, Mp, Cp);

    clf.cnbi.M = M;
    clf.cnbi.C = C;
    
    clf.bin.M = Mp;
    clf.bin.C = Cp;
    
end