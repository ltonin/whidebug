clear all; clc;

% Exporting Matlab CNBI Gaussian classifier to binary file. The classes are
% concatenated because Eigen don't use 3D array

rootresults  = './extra/data/';

ClassifierPath = '/mnt/data/Research/smr/20130725_AIAS_Robotino/CL05NA_rhlh_20121120_no_normalization.mat';

fM = [rootresults 'classifier_gaussian_M.dat'];
fC = [rootresults 'classifier_gaussian_C.dat'];
fP = [rootresults 'classifier_gaussian_parameters.dat'];

load(ClassifierPath);

M = analysis.tools.net.gau.M;
C = analysis.tools.net.gau.C;

Mp = permute(M, [3 2 1]);
Cp = permute(C, [3 2 1]);

P = cat(3, Mp, Cp);

disp(['Saving Gaussian classifier parameters at: ' fP]);
bcitk_save(fP, P);

% disp(['Saving Gaussian classifier centers at: ' fM]);
% bcitk_save(fM, Mp);
% 
% disp(['Saving Gaussian classifier covariances at: ' fC]);
% bcitk_save(fC, Cp);