clear all; clc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rootresults   = './extra/data/';

EEG_NSAMPLES      = 210432;
EEG_NCHANNELS     = 16;
BCI_PSD_NFFT      = 129;
BCI_PSD_SHIFT     = 32;
BCI_FRAME_TOTAL   = floor(EEG_NSAMPLES/BCI_PSD_SHIFT);
BCI_GAU_CLASSES   = 2;
BCI_GAU_FEATURES  = 5;
BCI_EXP_REJECTION = 0.6;
BCI_EXP_ALPHA     = 0.96;

fpsd   = [rootresults 'test_cnbisimloop_psd.dat'];
fgrid  = [rootresults 'test_cnbisimloop_grid.dat'];
ffeat  = [rootresults 'test_cnbisimloop_feat.dat'];
fpp    = [rootresults 'test_cnbisimloop_pp.dat'];
frpp   = [rootresults 'test_cnbisimloop_rpp.dat'];
fipp   = [rootresults 'test_cnbisimloop_ipp.dat'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('[io] - Open psd');
psd  = bcitk_load(fpsd, [BCI_PSD_NFFT EEG_NCHANNELS BCI_FRAME_TOTAL]);
mpsd = mean(psd, 3); 

disp('[io] - Open frequency grid');
sgrid = bcitk_load(fgrid, [1 BCI_PSD_NFFT], 'int');

disp('[io] - Open features');
feat = bcitk_load(ffeat, [BCI_FRAME_TOTAL BCI_GAU_FEATURES]);

disp('[io] - Open posterior probabilities');
pp = bcitk_load(fpp, [BCI_FRAME_TOTAL BCI_GAU_CLASSES]);

disp('[io] - Open rejected posterior probabilities');
rpp = bcitk_load(frpp, [BCI_FRAME_TOTAL BCI_GAU_CLASSES]);

disp('[io] - Open integrated posterior probabilities');
ipp = bcitk_load(fipp, [BCI_FRAME_TOTAL BCI_GAU_CLASSES]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cnbiroot    = '/mnt/data/Research/smr/20130725_AIAS_Robotino/';
fgdf        = [cnbiroot 'CL05NA.20130725.144932.online.mi.mi_rhlh.gdf'];
fclassifier = [cnbiroot 'CL05NA_rhlh_20121120_no_normalization.mat'];

bci = eegc3_smr_simloop(fgdf, [], fclassifier, BCI_EXP_REJECTION, BCI_EXP_ALPHA);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('[proc] - Computing goodness of fit for results');

bci.nfeats(isnan(bci.nfeats)) = 0;
goFfeat = goodnessOfFit(feat, bci.nfeats, 'NMSE');

bci.cprobs(isinf(bci.cprobs)) = 0;
goFpp   = goodnessOfFit(rpp, bci.cprobs, 'NMSE');

goFipp  = goodnessOfFit(ipp, bci.iprobs, 'NMSE');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gcf = figure;
fig_set_position(gcf, 'Top');

subplot(1, 3, 1);
[f, fId] = intersect(sgrid, bci.analysis.settings.features.psd.freqs);
plot(f, mpsd(fId, :));
grid on;
xlabel('Frequency [Hz]');
ylabel('Power [dB]');
title('Spectrum from pwelch computed by BCITK');

subplot(1, 3, 2);
bar(goFfeat);
grid on;
ylim([0 1.5]);
xlabel('Features');
ylabel('NMSE [-Inf 1]');
title('Goodness of fit between features');

subplot(1, 3, 3);
h = bar([min(goFpp); min(goFpp)]);
grid on;
ylim([0 1.5]);
xlim([0 3]);
set(gca, 'XTickLabel', {'pp', 'ipp'})
xlabel('Probabilities');
ylabel('NMSE [-Inf 1]');
title('Goodness of fit between probabilities');


