clear all; clc;

rootpath = '/mnt/data/Git/Codes/whitk/whicore/extra/data/';

fgau_mat  = [rootpath 'gaussian_classifier.mat'];
fgau_bin  = [rootpath 'gaussian_classifier.dat'];
fgau_fbin = [rootpath 'gaussian_classifier_file.dat'];
fgau_sbin = [rootpath 'gaussian_classifier_struct.dat'];

%% Saving gaussian classifier to binary (from file)
wc_save_classifier(fgau_mat, fgau_fbin);

%% Saving gaussian classifier to binary (from struct)
srcst = load(fgau_mat);
wc_save_classifier(srcst, fgau_sbin);

%% Loading binary gaussian classifier (saved from file)
[hdr, M, C] = wc_load_classifier(fgau_fbin);
disp(hdr);
disp(M);
disp(C);

%% Loading binary gaussian classifier (saved from struct)
[hdr, M, C] = wc_load_classifier(fgau_sbin);
disp(hdr);
disp(M);
disp(C);



