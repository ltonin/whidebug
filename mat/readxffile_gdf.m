clear all; clc;
root = './extra/data/';

EEGCH = 1:16;
TRICH = 17;

NEEG = length(EEGCH);
NTRI = length(TRICH);

filegdf = [root 'test_readxdffile.gdf'];
eegpath_c = [root 'test_readxdffile_gdf_eeg.dat'];
exgpath_c = [root 'test_readxdffile_gdf_exg.dat'];
tripath_c = [root 'test_readxdffile_gdf_tri.dat'];

display('[io] - Loading GDF file');
[s, h] = sload(filegdf);
eeg_gdf = s(:, EEGCH);
tri_gdf = s(:, TRICH);
NSAMPLES = size(eeg_gdf, 1);

display('[io] - Loading C file');
eeg_c = bcitk_load(eegpath_c, [NEEG NSAMPLES], 'float');
tri_c = bcitk_load(tripath_c, [NTRI NSAMPLES], 'uint32');


disp('[check] - Goodness of fit for EEG channels');
eeg_gfit = zeros(NEEG, 1);
tri_gfit = zeros(NTRI, 1);

for chId = 1:NEEG
    eeg_gfit(chId) = goodnessOfFit(eeg_gdf(:, chId), eeg_c(chId, :)', 'NMSE'); 
end

for chId = 1:NTRI
    tri_gfit(chId) = goodnessOfFit(tri_gdf(:, 1), tri_c(chId, :)', 'NMSE'); 
end

plot(EEGCH, eeg_gfit);
grid on;
xlabel('Channels');
ylabel('NMSE');
title('Goodness of fit for EEG channels');

% subplot(1, 3, 2);
% plot(TRICH, tri_gdf);
% grid on;
% xlabel('Channels');
% ylabel('NMSE');
% title('Goodness of fit for TRI channels');
