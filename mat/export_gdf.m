clear all; clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rootresults  = './extra/data/';

fgdf   = '/mnt/data/Research/smr/20130725_AIAS_Robotino/CL05NA.20130725.144932.online.mi.mi_rhlh.gdf';
fdest  = [rootresults 'test_cnbisimloop_eeg.dat']; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('[io] - Open gdf signal');
s = sload(fgdf);
s = s(:, 1:end-1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Saving gdf file at: ' fdest]);
bcitk_save(fdest, s);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%