clc; clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rootresults   = './extra/data/';
TimeLabels = {'NDFRead',     'BufferAdd', 'BufferGet',  'Laplacian', ...
	          'Pwelch',      'Features',  'Classifier', 'Rejection', ...
	          'Integration', 'IcComm',    'IdComm',     'Total'};

frtimes = [rootresults 'cnbismr_debug_times.dat'];

NumTotSamples = 10000;
NumTotTimes   = 12;
NumSamples = 3595;
NumTimes   = 11;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('[io] - Open debug times');
itimes = bcitk_load(frtimes, [NumTotSamples NumTotTimes]);
times = itimes(1:NumSamples, 1:NumTimes);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DiffTimes = diff(times, [], 2);
AvgTimes = mean(DiffTimes, 1);
StdTimes = std(DiffTimes, 1);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;

subplot(3, 4, 1);
hist(times(:, end), 20);
title(TimeLabels{end});
xlabel('[ms]');
ylabel('#');
xlim([55 75]);
grid on;

subplot(3, 4, 2);
hist(times(:, 1), 20);
title(TimeLabels{1});
xlabel('[ms]');
ylabel('#');
xlim([55 75]);
grid on;

for tId = 2:size(DiffTimes, 2)
    subplot(3, 4, tId + 1); 
    hist(DiffTimes(:, tId), 20);
    title(TimeLabels{tId+1});
    xlabel('[ms]');
    ylabel('#');
    grid on;
end

figure;
plot_barweb(AvgTimes, StdTimes, [], [], 'Time delay', 'Operations', '[ms]', [], 'xy', TimeLabels(2:end-1), [], [])
