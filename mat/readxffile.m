clear all; clc;
root = './extra/data/';

EEGCH = 1:64;
EXGCH = 65:88;
TRICH = 89;

NEEG = length(EEGCH);
NEXG = length(EXGCH);
NTRI = length(TRICH);

filebdf = [root 'test_readxdffile.bdf'];
eegpath_c = [root 'test_readxdffile_bdf_eeg.dat'];
exgpath_c = [root 'test_readxdffile_bdf_exg.dat'];
tripath_c = [root 'test_readxdffile_bdf_tri.dat'];

display('[io] - Loading BDF file');
h_bdf = readbdfheader(filebdf);
eeg_bdf = readbdfdata(h_bdf, 0, EEGCH);
exg_bdf = readbdfdata(h_bdf, 0, EXGCH);
tri_bdf = readbdfdata(h_bdf, 0, {'Status'});
NSAMPLES = size(eeg_bdf, 2);

display('[io] - Loading C file');
eeg_c = bcitk_load(eegpath_c, [NEEG NSAMPLES], 'float');
exg_c = bcitk_load(exgpath_c, [NEXG NSAMPLES], 'float');
tri_c = bcitk_load(tripath_c, [NTRI NSAMPLES], 'uint32');


disp('[check] - Goodness of fit for EEG channels');
eeg_gdf = zeros(NEEG, 1);
exg_gdf = zeros(NEXG, 1);
tri_gdf = zeros(NTRI, 1);
triv_gdf = zeros(NTRI, 1);
triv_bdf = zeros(NTRI, NSAMPLES);
triv_c = zeros(NTRI, NSAMPLES);

for chId = 1:NEEG
    eeg_gdf(chId) = goodnessOfFit(eeg_bdf(chId, :)', eeg_c(chId, :)', 'NMSE'); 
end

for chId = 1:NEXG
    exg_gdf(chId) = goodnessOfFit(exg_bdf(chId, :)', exg_c(chId, :)', 'NMSE'); 
end

for chId = 1:NTRI
    triv_bdf(chId, :) = (bitand(hex2dec('FF'), tri_bdf(chId, :)));
    triv_c(chId, :) = (bitand(hex2dec('FF'), tri_c(chId, :)));
    tri_gdf(chId) = goodnessOfFit(tri_bdf(chId, :)', tri_c(chId, :)', 'NMSE'); 
    triv_gdf(chId) = goodnessOfFit(triv_bdf(chId, :)', triv_c(chId, :)', 'NMSE'); 
end


subplot(1, 3, 1);
plot(EEGCH, eeg_gdf);
grid on;
xlabel('Channels');
ylabel('NMSE');
title('Goodness of fit for EEG channels');

subplot(1, 3, 2);
plot(EXGCH, exg_gdf);
grid on;
xlabel('Channels');
ylabel('NMSE');
title('Goodness of fit for EEG channels');

subplot(1, 3, 3);
hold on;
plot(triv_bdf, 'b');
plot(triv_c, 'r');
hold off;
grid on;
xlabel('Samples');
ylabel('NMSE');
title('Triggers value');
legend('From bdf', 'From C');