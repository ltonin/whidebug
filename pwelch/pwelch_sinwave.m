clear all; clc;
root  = [getenv('WHICORE') 'extra/pwelch/'];
                                      
PSD_WINSIZE   = 256;
PSD_NOVL      = 128;
PSD_NFFT 	  = (PSD_WINSIZE/2.0) + 1;
PSD_SR        = 512;

fsin  = [root 'pwelch_sin.dat'];
fpsd  = [root 'pwelch_psd.dat'];
fgri  = [root 'pwelch_gri.dat'];

disp('[io] - Open input signal');
sinw = wc_load(fsin, 'eigen', 'double');

disp('[io] - Open psd signal');
psd = wc_load(fpsd, 'eigen', 'double');

disp('[io] - Open psd grid');
gri = wc_load(fgri, 'vector', 'uint32');

SIN_NSAMPLES  = size(sinw, 1);
SIN_NCHANNELS = size(sinw, 2);

disp('==========================');
disp('Applying matlab pwelch');
disp('==========================');
psd_m = zeros(PSD_NFFT, SIN_NCHANNELS);

for chId = 1:SIN_NCHANNELS
    [psd_m(:, chId), gri_m] = pwelch(sinw(:, chId), hann(PSD_WINSIZE), PSD_NOVL, [], PSD_SR);
end

disp('==========================');
disp('Check goodness of fit');
disp('==========================');

disp('[chk] - Pwelch');
for chId = 1:SIN_NCHANNELS
    disp(['[chk] - Goodness of fit for channel ' num2str(chId) ': ' num2str(goodnessOfFit(psd(:, chId), psd_m(:, chId), 'NMSE'))]);
end

figure; 
subplot(1, 2, 1);
plot(gri, psd);
title('Pwelch computed in C++');
grid on;

subplot(1, 2, 2);
plot(gri_m, psd_m);
title('Pwelch computed in Matlab');
grid on;